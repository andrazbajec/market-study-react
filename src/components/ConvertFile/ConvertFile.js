import React from 'react';

const convertFile = props => {
    let file = props.getFileState.file;
    let counter = 0;
    try {
        file = JSON.parse(file);
        props.setFileState({
            ...props.getFileState,
            file: file
        });
    } catch(e) {
        const fileLines = file.split("\n");
        const newFile = [];
        try {
            fileLines.map(line => {
                if(line !== "") {
                    newFile.push(JSON.parse(line));
                }
            });
            props.setFileState({
                ...props.getFileState,
                file: newFile
            });
        } catch(e) {
            console.log(e);
        }
    }

    return (
        <div>Converting file..</div>
    )
}

export default convertFile;