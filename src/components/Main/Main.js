import React, { useState } from 'react';
import GenerateElements from '../GenerateElements/GenerateElements';
import Button from '../Button/Button';
import './Main.css';

const Main = props => {
    const [getMainState, setMainState] = useState({
        ...props.currentLine,
        counter: 0
    });

    const [getLocalState, setLocalState] = useState({
        ...getMainState[getMainState.counter]
    });

    // Test, to see, which of the two JSON types the given file is //
    if(Object.keys(getLocalState).length === 0) {
        setLocalState({
            ...getMainState
        });
    }

    // Return the length of an object //
    const objLength = obj => {
        return Object.keys(obj).length
    }

    // Go to previous/next element in the JSON //
    const changeElement = num => {
        const counter = getMainState.counter + num;
        if(counter >= 0 && counter < objLength(getMainState.file)) {
            setMainState({
                ...getMainState,
                counter: counter
            });
        }
    }

    // Update editable components //
    const updateEditableElement = (el, key) => {
        const val = el.target.value;
        const state = {...getLocalState};
        state.file[getMainState.counter]._source[key] = el.target.value;
        setLocalState({
            ...state
        });
    }

    return(
        <div className="Main">
            <GenerateElements file={{...getLocalState.file}[getMainState.counter]}/>
            <a id="downloadLink">Download</a>
            <div className="cls" />
            <hr />
            <GenerateElements 
                file={{...getLocalState.file}[getMainState.counter]} 
                editable 
                onChange={updateEditableElement} 
            />
            <div className="cls" />
            <Button 
                color="blue"
                disabled={getMainState.counter === 0}
                text="Previous"
                onClick={() => changeElement(-1)}
            />
            <Button 
                color="blue"
                disabled={getMainState.counter === objLength(getMainState.file) - 1}
                text="Next"
                onClick={() => changeElement(+1)}
            />
            <Button
                color="green"
                text="Save"
            />
        </div>
    );
}

export default Main;