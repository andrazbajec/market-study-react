import React from 'react';
import './Button.css';

const button = props => {
    let btnClass = props.color + "Btn";
    return(
        <button 
            className={btnClass} 
            disabled={props.disabled}
            onClick={props.onClick}
        >
            {props.text}
        </button>
    );
}

export default button;