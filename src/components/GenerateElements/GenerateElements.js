import React from 'react';
import Element from '../Element/Element';
import shownKeys from '../ShownKeys/ShownKeys';
import editableShownKeys from '../ShownKeys/EditableShownKeys';

const generateElements = props => {
    const keys = props.editable ? editableShownKeys: shownKeys;
    return (
        <div className="GenerateElements">
            {
                props.file !== undefined ?
                    keys.map(el => {
                        const title = el.title;
                        let text = props.file._source[el.text];
                        if(typeof text === "object" && text.country) {
                            text = text.country;
                        }
                        return <Element
                            title={title}
                            text={text}
                            key={el.key}
                            passKey={el.key}
                            type={props.editable ? "editable" : "non-editable"}
                            onChange={props.onChange}
                        />
                    }) :
                null
            }
        </div>
    );
}

export default generateElements;