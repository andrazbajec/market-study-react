import React, { useState} from 'react';

const ImportFile = props => {
    const fileStatusChange = evt => {
        const file = evt.target.files[0];
        var reader = new FileReader();
        reader.readAsText(file);
        let fileData = null;
        reader.onload = () => {
            props.setFileState({
                ...props.getFileState,
                file: reader.result
            });
        }
    }

    return(
        <div className="ImportFile">
            <input type="file" onChange={fileStatusChange} />
            <input type="submit" onClick={fileStatusChange} />
        </div>
    );
}
  

export default ImportFile;