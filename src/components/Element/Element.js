import React from 'react';
import './Element.css'

const element = props => {
    // Return the requested element with the required attributes //
    const returnElement = () => {
        if(props.type === "non-editable") {
            return <p 
                className="text"
            >
                {props.text}
            </p>;
        } else if(props.type === "editable") {
            return <input type="text" 
                className="textarea" 
                value={props.text}
                onChange={(el) => props.onChange(el, props.passKey)}
            />
        } else {
            return null;
        }
    }

    return(
        <div className="Element">
            <span className="title">{props.title}</span>
            {returnElement()}
        </div>
    )
}

export default element;