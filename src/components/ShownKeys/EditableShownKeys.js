export default [
    { title: "Brand",   text: "Brand",      key: "Brand"},
    { title: "Model",   text: "Model",      key: "Model"},
    { title: "Version", text: "Version",    key: "Version"}
];