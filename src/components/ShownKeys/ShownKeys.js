export default [
    { title: "Ad title",            text: "Name",                       key:"Name"},
    { title:  "Registration date",  text: "DateOfFirstRegistration",    key: "DateOfFirstRegistration" },
    { title: "HP",                  text: "HorsePower",                 key: "HorsePower" },
    { title: "Transmission",        text: "Transmission",               key: "Transmission" },
    { title: "Brand",               text: "RawBrand",                   key: "RawBrand" },
    { title: "Engine",              text: "Engine",                     key: "Engine" },
    { title: "Fuel Type",           text: "FuelType",                   key: "FuelType" },
    { title: "URL",                 text: "URL",                        key: "URL" },
    { title: "Model",               text: "RawModel",                   key: "RawModel" },
    { title: "Engine Capacity",     text: "EngineCapacity",             key: "EngineCapacity" },
    { title: "Drive Train",         text: "DriveTrain",                 key: "DriveTrain" },
    { title: "Country",             text: "Country",                    key: "Country"}
];