import React, { useState } from 'react';
import './App.css';
import Main from './components/Main/Main';
import ImportFile from './components/ImportFile/ImportFile';
import ConvertFile from './components/ConvertFile/ConvertFile';

function App() {
  const [getFileState, setFileState] = useState({
    file: null,
    currentLine: 0
  });

  return (
    <div className="App">
      {
        getFileState.file ?
          typeof getFileState.file === "object" ?
            <Main 
              getFileState={getFileState}
              setFileState={setFileState}
              currentLine={getFileState.file[0] === undefined ? {file: [getFileState.file], currentLine: getFileState.currentLine} : getFileState}
            /> :
            <ConvertFile 
              getFileState={getFileState}
              setFileState={setFileState}
            />:
          <ImportFile 
            getFileState={getFileState}
            setFileState={setFileState}
          />
      }
    </div>
  );
}

export default App;
